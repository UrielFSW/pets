$(function (){
    console.log( $( '#fixMenu' ));
  // Get the header with me, all the time..
    var nav = $( '#fixMenu' )[0],
        top = $( nav ).offset().top,
        left,
        height = $( nav ).outerHeight(),
        width = $( nav ).width(),
        fixedClass = 'fixed';
        //$hiddenLogo = $(".ico-une-mini-logo"),
        //$userActions = $(".user-options-actions");

    //$hiddenLogo.hide();    

    function pageOffset() {
        return window.pageYOffset || document.body.scrollTop;
    }
    // console.log($("#fixMenu").is(":visible"));
        $( window ).
        resize( function () {
            left = $( nav ).offset().left;

        }).
        scroll( function () {
            $( nav ).toggleClass( fixedClass, pageOffset() > top );
            //$("#headerSections").toggleClass( fixedClass, pageOffset() > top );

            if ( $( nav ).hasClass( fixedClass ) ) {
                //$hiddenLogo.show();
                //$userActions.addClass("user-actions-fixed");     
                $( nav ).
                    css({ 'left': left, 'width': "100%" }).
                    prev().css({ 'marginBottom': height });
                $( nav ).find("navbar").
                    css({ 'left': left, 'width': "100%" }).
                    prev().css({ 'marginBottom': $( nav ).find("navbar").outerHeight() });
            } else {
                //$hiddenLogo.hide();
                //$userActions.removeClass("user-actions-fixed");    
                $( nav ).
                    prev().andSelf().removeAttr( 'style' );
                $( nav ).find("navbar").
                    prev().andSelf().removeAttr( 'style' );   
            }
        }).
        trigger( 'resize' );
});
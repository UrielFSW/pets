$(document).ready(function () {
    var wantedInsurance = [];
    $(document).on("change", ".check-list input[type=checkbox]", function () {
        $(this).parent().next(".radio-list").slideToggle();
        var newVal = $(this).val();
        var found = $.inArray(newVal, wantedInsurance);
        if (found >= 0) {
            wantedInsurance.splice(found, 1);
        } else {
            wantedInsurance.push(newVal);
        }
    });
    if ($(".validate-form").length) {
        $(".validate-form").each(function () {
            var form = $(this);
            $(this).validate({
                submitHandler: function (form) {
                    data = $(form).serialize();
                    $.ajax({
                        type: $(form).attr('method'),
                        url: $(form).attr('action'),
                        data: data,
                        dataType: 'json'
                    }).done(function (response) {
                        if (response.status === 'ok') {
                            $(".last-step").find('.step-content').show();
                            $(".last-step").prevAll(".step-wrap").hide();
                            $(".last-step").show();
                            if (wantedInsurance !== []) {
                                var i = 0;
                                var j = wantedInsurance.length - 1;
                                for (i=0;i<j;i++){
                                    $(".proposal-wrap").eq(0).clone().appendTo(".proposals-wrap").addClass('p-'+i);
                                };
                                for (i=0;i<=j;i++){
                                    $('.proposal-wrap').eq(i).find(".proposal-title").html("<h1><span class='ico-wrap ico-"+wantedInsurance[i]+"'></span>"+wantedInsurance[i]+"</h1>");
                                };
                            }

                            $(".box-wrap").each(function (index, elem) {
                                if ($(this).data('plan') === response.opcion) {
                                    $(this).addClass("recommended");
                                }
                                if (response.opcion === 'Premium') {
                                    $(".plan-wrap-1").addClass("col-xs-push-8");
                                    $(".plan-wrap-2").addClass("col-xs-pull-8");
                                }
                            });
                        } else {
                            console.log("ERROR");
                        }
                    });
                }
            });
        });
    }

    if ($(".slider-content").length) {
        $(".slider-content").each(function () {
            $(this).bxSlider({
                mode: $(this).data('mode') || 'horizontal',
                slideWidth: $(this).data('slide-width') || 0,
                maxSlides: $(this).data('max-slides') || 1,
                minSlides: $(this).data('max-slides') || 1,
                auto: hasData($(this).data('auto')) ? $(this).data('auto') : false || true,
                pager: hasData($(this).data('pager')) ? $(this).data('pager') : false || true,
                controls: hasData($(this).data('controls')) ? $(this).data('controls') : false || true,
                speed: $(this).data('speed') || 800,
                moveSlides: 1
            });
        });
    }
    $(".step-header").click(function () {
        $(this).parent().siblings().find('.step-header').next(".step-content").slideUp();
        $(this).next(".step-content").slideDown();
    });
    //$(".fancybox").fancybox();
    $(".options-content a").click(function () {
        var parent = $(this).closest(".step-wrap");
        var step = $(this).closest(".step-content");
        var option = $(this).closest(".options-content");
        option.find("a").removeClass('active');
        $(this).addClass('active');
        var goTo = $(this).data('go-to');
        var nextStep = null;
        if ($(this).data('hide') !== '') {
            $(".step-wrap[data-step-name='" + $(this).data('hide') + "']").slideUp();
        }
        $(".step-wrap").each(function () {
            if ($(this).data('step-name') === goTo) {
                nextStep = $(this);
                step.slideUp();
                nextStep.slideDown();
                nextStep.children().slideDown();
            }
        });
        var environment = '';
//        var environment = '/index_dev.php';


        if ($(this).attr('data-load')) {
            var q = $(this).attr('data-load');
            $(".user-questions-wrap").load(environment + "/q/" + q, function () {
                wantedInsurance = [];
            });
        }

        if ($(this).attr('data-edad')) {
            $("input[name='edad']").val($(this).data('edad'));
            $(".span-edad").html($(this).data('edad'));
        } else if ($(this).attr('data-estadocivil')) {
            $("input[name='estadocivil']").val($(this).data('estadocivil'));
            $(".span-estado-civil").html($(this).data('estadocivil'));
        } else if ($(this).attr('data-sexo')) {
            $("input[name='sexo']").val($(this).data('sexo'));
            $(".span-sexo").html($(this).data('sexo'));
        }

        return false;
    });
    $('#reload').click(function () {
        location.reload();
    });
});
function hasData(variable) {
    return  typeof (variable) !== undefined && variable !== null ? true : false;
}

